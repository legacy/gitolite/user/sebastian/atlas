// ~ main.js ~
// Main GLClient starter
// based on work done by @jrburke
// Configure require.js shortcut aliases
require.config({
  shim : {
    "bootstrap" : { "deps" :['jquery'] }
  },
  paths: {
    jquery: '/js/jquery-3.2.1.min',
    underscore: '/rs/js/libs/underscore/underscore-min',
    backbone: '/rs/js/libs/backbone/backbone.min',
    text: '/rs/js/libs/require/text',
    datatables: '/rs/js/libs/datatables/jquery.dataTables.min',
    datatablest: '/rs/js/libs/datatables/dataTables.TorStatus',
    datatablessort: '/rs/js/libs/datatables/dataTables.Sorting',
    bootstrap: '/rs/js/libs/bootstrap/bootstrap.min',
    datatablesbs: '/rs/js/libs/datatables/dataTables.bootstrap',
    d3js: '/rs/js/libs/d3js/d3.v3.min',
    "d3-geo-projection": '/rs/js/libs/d3js/d3-geo-projection.v2.min',
    "d3-geo": '/rs/js/libs/d3js/d3-geo.v1.min',
    "d3-array": '/rs/js/libs/d3js/d3-array.v1.min',
    topojson: '/rs/js/libs/d3js/topojson.v1.min',
    jssha: '/rs/js/libs/jssha/sha1',
    templates: '/rs/templates',
    fallbackdir: '/rs/js/fallback_dir'
  }

});

require([

  // Load our app module and pass it to our definition function
  'app'

  // Some plugins have to be loaded in order due to their non AMD compliance
  // Because these scripts are not "modules" they do not pass any values to the definition function below
], function(App){
  // The "app" dependency is passed in as "App"
  // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
  App.initialize();
});
